from django_cron import CronJobBase, Schedule
from ant.apps.transactions.models import PendingPayment
from ant.apps.accounts.models import Account
from ant.apps.transactions.models import Transaction
from ant.apps.categories.models import Category
from datetime import date

class CheckUserTransactionsJob(CronJobBase):
    RUN_EVERY_MINS = 5 # every 2 hours

    schedule = Schedule(run_every_mins=RUN_EVERY_MINS)
    code = 'ant.check_user_transactons'    # a unique code

    def do(self):
        # print('cronjob running')
        # TODO: check user's pending payments and then check their banks' transactions
        accounts = Account.objects.filter(is_archived=True).all()
        for account in accounts:
            transactions = account.get_account_transactions(start_date=date.today())
            if transactions is not None:
                for transaction in transactions:
                    if not Transaction.objects.filter(lean_account_id=transaction.get("id")).exists():
                        if transaction.get("amount") > 0:
                            transaction_type = Transaction.TransactionType.INCOME
                        else:
                            transaction_type = Transaction.TransactionType.EXPENSE
                        category, created = Category.objects.get_or_create(
                            lean_category_name=transaction.get("insights", {}).get(
                                "category", None
                            )
                        )
                        Transaction.objects.create(
                            title=transaction.get("description"),
                            user=account.user,
                            account=account,
                            transaction_type=transaction_type,
                            timestamp=transaction.get("timestamp"),
                            category=category,
                            amount=transaction.get("amount"),
                            currency=account.currency,
                            lean_transaction_id=transaction.get("id"),
                        )
