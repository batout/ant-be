from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.decorators import api_view, permission_classes
from datetime import date
from django.utils.translation import ugettext_lazy as _

from ant.apps.accounts.models import Account
from ant.apps.banks.models import Bank
from ant.apps.categories.models import Category
from ant.apps.transactions.models import Transaction, PendingPayment
from ant.apps.users.models import User, UserEntity
from ant.apps.budgets.models import Budget
from ant.apps.notifications.models import Notification
from ant.apps.budgets.api.v1.serializers import BudgetSerializer
from ant.apps.notifications.api.v1.services import NotificationServices
class WebHookGenericView(APIView):
    """
    Generic view for webhooks
    """
    permission_classes = [AllowAny]

    def post(self, request, *args, **kwargs):
        """
        POST method invoked by lean tech webhooks
        """

        t = self.request.data.get("type")
        if t == "entity.created":
            payload = self.request.data.get("payload")
            customer_id = payload.get("customer_id")
            user = User.objects.filter(lean_customer_id=customer_id).last()
            bank = Bank.objects.filter(
                identifier=payload.get("bank_details").get("identifier")
            ).last()
            user_entity = UserEntity.objects.create(
                user=user,
                bank=bank,
                entity_id=payload.get("id"),
            )
            accounts = user_entity.get_lean_accounts()
            for account in accounts:
                account = Account.objects.create(
                    user=user,
                    bank=bank,
                    entity=user_entity,
                    lean_account_id=account.get("account_id"),
                )
                transactions = account.get_account_transactions()
                for transaction in transactions:
                    if transaction.get("amount") > 0:
                        transaction_type = Transaction.TransactionType.INCOME
                    else:
                        transaction_type = Transaction.TransactionType.EXPENSE
                    category, created = Category.objects.get_or_create(
                        lean_category_name=transaction.get("insights", {}).get(
                            "category", None
                        )
                    )

                    Transaction.objects.create(
                        title=transaction.get("description"),
                        user=user,
                        account=account,
                        transaction_type=transaction_type,
                        timestamp=transaction.get("timestamp"),
                        category=category,
                        amount=transaction.get("amount"),
                        currency=account.currency,
                        lean_transaction_id=transaction.get("id"),
                    )
        return Response({}, status=status.HTTP_200_OK)

    def get(self, request, *args, **kwargs):
        return Response({}, status=status.HTTP_200_OK)



@api_view(['POST'])
@permission_classes([AllowAny])
def check_user_budget(request):
    """
        POST method invoked by lean tech webhooks when payment created
    """
    request_type = request.data.get("type")
    n_controller = NotificationServices()
    if request_type == "payment.created":
        # get body of the payment and get the type ot transaction
        payload = request.data.get("payload")
        customer_id = payload.get("customer_id")
        user = User.objects.filter(lean_customer_id=customer_id).last()

        accounts = Account.objects.filter(user=user).all()
        recheck_transaction = True
        for account in accounts:
            transactions = account.get_account_transactions(start_date=date.today())
            last_transction = Transaction.objects.filter(account=account, user=user).last()
            if transactions[-1].get("id") != last_transction.lean_account_id:
                recheck_transaction = False
                if transactions[-1].get("amount") > 0:
                    transaction_type = Transaction.TransactionType.INCOME
                else:
                    transaction_type = Transaction.TransactionType.EXPENSE
                category, created = Category.objects.get_or_create(
                    lean_category_name=transactions[-1].get("insights", {}).get(
                        "category", None
                    )
                )
                # need to check the category and budget limit
                Transaction.objects.create(
                    title=transactions[-1].get("description"),
                    user=user,
                    account=account,
                    transaction_type=transaction_type,
                    timestamp=transactions[-1].get("timestamp"),
                    category=category,
                    amount=transactions[-1].get("amount"),
                    currency=account.currency,
                    lean_transaction_id=transactions[-1].get("id"),
                )
                budgets = Budget.objects.filter(user=user).all()
                check_category_budget = False
                for budget in budgets :
                    if category in budget.categories.all():
                        check_category_budget = True
                        expenses = BudgetSerializer.get_expenses(budget)
                        if (expenses/budget.amount*100) >= 90:
                            n_controller.create_notification(title=_('You Excee 90% of budget'), body=_('You Excee 90% of budget'),notification_type=Notification.NotificationType.SPECIFIC, user=user)
                if not check_category_budget :
                    n_controller.create_notification(title=_('You don\'t have budget for this transaction'), body=_('You don\'t have budget for this transaction'),notification_type=Notification.NotificationType.SPECIFIC, user=user)
                return Response({"message": "Transaction found and notify user is applied!"})
        if recheck_transaction :
            PendingPayment.objects.create(
                lean_payment_id=payload.get("id"), 
                payment_status=payload.get("status"),
                lean_customer_id=payload.get("customer_id")
            )
            
        return Response({"message": "Transaction not found!"})
