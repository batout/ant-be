from django.urls import path
from ant.apps.webhooks.api.v1.views import WebHookGenericView, check_user_budget
# from rest_framework import routers

app_name = "webhooks"

urlpatterns = [
    path("", WebHookGenericView.as_view()),
    path("check_user_budget", check_user_budget),
]
