# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Currency


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "name",
        "code",
        "country",
    )
    list_filter = ("added_at", "updated_at", "country")
    search_fields = ("name",)
    date_hierarchy = "updated_at"
