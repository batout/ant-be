from django.db import models

from ant.apps.users.models import Country
from ant.utils.models import BaseModel


class Currency(BaseModel):
    name = models.CharField(max_length=100, null=True)
    code = models.CharField(max_length=4)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name if self.name is not None else "?"
