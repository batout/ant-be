from rest_framework import viewsets
from rest_framework.permissions import AllowAny

from ant.apps.currencies.api.v1.serializers import CurrencySerializer
from ant.apps.currencies.models import Currency


class CurrencyViewSet(viewsets.ModelViewSet):
    permission_classes = [AllowAny]
    serializer_class = CurrencySerializer
    queryset = Currency.objects.all().select_related('country')
