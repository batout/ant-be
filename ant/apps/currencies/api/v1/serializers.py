from ant.apps.currencies.models import Currency
from ant.utils.serializers import ModelSerializer


class CurrencySerializer(ModelSerializer):
    class Meta:
        model = Currency
        fields = [
            "id",
            "name",
            "code",
            "country",
        ]
