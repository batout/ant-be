from rest_framework import routers

from ant.apps.currencies.api.v1.views import CurrencyViewSet

router = routers.DefaultRouter()

app_name = "currencies"
router.register("currencies", CurrencyViewSet)

urlpatterns = router.urls
