# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Account


@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "title",
        "description",
        "user",
        "account_type",
        "opening_balance",
        "include_in_net_worth",
        "bank",
        "is_archived",
        "currency",
        "entity_id",
    )
    list_filter = (
        "added_at",
        "updated_at",
        "user",
        "include_in_net_worth",
        "bank",
        "is_archived",
        "currency",
    )
    date_hierarchy = "updated_at"
