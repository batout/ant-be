from django.conf import settings
from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from ant.apps.banks.models import Bank
from ant.apps.currencies.models import Currency
from ant.apps.users.models import User, UserEntity
from ant.utils.models import BaseModel


class Account(BaseModel):
    class AccountType(models.TextChoices):
        CASH = "CASH", _("Cash")
        BANK_CHECKING = "BANK_CHECKING", _("Bank checking")
        BANK_SAVING = "BANK_SAVING", _("Bank saving")
        BANK_CREDIT = "BANK_CREDIT", _("Bank credit")
        LOAN = "LOAN", _("Loan")
        LENDING = "LENDING", _("Lending")

    title = models.CharField(max_length=50)
    description = models.TextField(max_length=1000, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account_type = models.CharField(choices=AccountType.choices, max_length=13)
    opening_balance = models.DecimalField(decimal_places=2, max_digits=14, null=True)
    include_in_net_worth = models.BooleanField(default=True)
    bank = models.ForeignKey(Bank, on_delete=models.SET_NULL, null=True, blank=True)
    is_archived = models.BooleanField(default=False)
    currency = models.ForeignKey(
        Currency, on_delete=models.SET_NULL, null=True, blank=True
    )
    entity = models.ForeignKey(
        UserEntity, on_delete=models.SET_NULL, null=True, blank=True
    )
    lean_account_id = models.UUIDField(null=True)

    def save(self, *args, **kwargs):
        if self.opening_balance is None and self.account_type != self.AccountType.CASH:
            (
                balance,
                currency_code,
                account_name,
                account_type,
            ) = self.get_lean_account_balance_data()
            self.opening_balance = balance
            self.currency, created = Currency.objects.get_or_create(code=currency_code)
            self.title = account_name
            self.account_type = account_type
        if self.opening_balance is None:
            self.opening_balance = 0
        super(Account, self).save(*args, **kwargs)

    def __str__(self):
        return self.title

    def get_lean_account_balance_data(self):
        """
        get balance for account from lean apis
        :return: balance , currency_code, account_name, account_type
        """
        import json

        import requests

        url = f"{settings.LEAN_BASE_URL}v1/balance"

        payload = json.dumps(
            {"entity_id": self.entity.entity_id, "account_id": self.lean_account_id}
        )
        headers = {
            "Content-Type": "application/json",
            "lean-app-token": settings.LEAN_AUTHENTICATION_TOKEN,
        }

        response = requests.request("POST", url, headers=headers, data=payload)
        payload = response.json().get("payload", {})
        return (
            payload.get("balance"),
            payload.get("currency_code"),
            payload.get("account_name"),
            payload.get("account_type"),
        )

    def get_account_transactions(self, start_date=None, end_date=None):
        import json

        import requests

        url = f"{settings.LEAN_BASE_URL}v1/transactions"
        if self.entity is not None and self.lean_account_id is not None:
            data = {
                "entity_id": self.entity.entity_id,
                "account_id": self.lean_account_id,
                "insights": True,
            }
            if start_date is not None:
                data["start_date"] = start_date
            if end_date is not None:
                data["end_date"] = end_date

            payload = json.dumps(data)
            headers = {
                "Content-Type": "application/json",
                "lean-app-token": settings.LEAN_AUTHENTICATION_TOKEN,
            }

            response = requests.request("POST", url, headers=headers, data=payload)
            return response.json().get("payload", {}).get("transactions", [])
        else:
            return None
