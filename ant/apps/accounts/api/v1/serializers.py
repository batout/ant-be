from rest_framework import serializers

from ant.apps.accounts.models import Account
from ant.apps.banks.api.v1.serializers import BankSerializer
from ant.apps.currencies.api.v1.serializers import CurrencySerializer
from ant.utils.serializers import ModelSerializer


class AccountSerializer(ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    bank = BankSerializer(read_only=True)
    currency = CurrencySerializer(read_only=True)

    class Meta:
        model = Account
        fields = [
            "id",
            "title",
            "description",
            "user",
            "account_type",
            "opening_balance",
            "include_in_net_worth",
            "bank",
            "is_archived",
            "currency",
        ]


class UserAccountsSummarySerializer(ModelSerializer):
    bank = BankSerializer()
    total_balance = serializers.FloatField()
