from django.utils.translation import ugettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.response import Response

from ant.apps.accounts.api.v1.serializers import AccountSerializer
from ant.apps.accounts.models import Account
from ant.apps.banks.api.v1.serializers import BankSerializer

class AccountViewSet(viewsets.ModelViewSet):
    serializer_class = AccountSerializer
    queryset = Account.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = [
        "is_archived",
        "account_type",
    ]

    def get_queryset(self, *args, **kwargs):
        return super(AccountViewSet, self).get_queryset().filter(user=self.request.user).select_related('bank','currency').only("id", "title","description", "account_type","opening_balance", "include_in_net_worth","bank", "is_archived","currency","bank__id", "bank__identifier", "bank__name", "bank__logo", "bank__country_code", "bank__active", "bank__main_color", "bank__background_color","currency__id","currency__name","currency__code","currency__country")
    
    @action(
        methods=[
            "get",
        ],
        detail=False,
    )
    def last_added(self, request, *args, **kwargs):
        user = self.request.user
        entity = user.entities.all().order_by("id").first()
        if entity is None:
            raise NotFound(_("User don't have accounts yet"))

        total_balance = 0
        for account in Account.objects.filter(entity=entity, user=user):
            total_balance += account.opening_balance

        return Response(
            {"total_balance": total_balance, "bank": BankSerializer(entity.bank).data},
            status=status.HTTP_200_OK,
        )
