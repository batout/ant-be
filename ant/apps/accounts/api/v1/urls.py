from rest_framework import routers

from ant.apps.accounts.api.v1.views import AccountViewSet

app_name = "accounts"

router = routers.DefaultRouter()
router.register("accounts", AccountViewSet)

urlpatterns = router.urls
