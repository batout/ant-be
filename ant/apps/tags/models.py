from django.db import models

# Create your models here.
from ant.apps.users.models import User
from ant.utils.models import BaseModel


class Tag(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=100)
    color = models.CharField(max_length=7)

    def __str__(self):
        return self.name
