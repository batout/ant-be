# code
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import Transaction
from .tasks import check_transaction_notify

@receiver(post_save, sender=Transaction)
def create_transaction(sender, instance, created, **kwargs):
	# print(instance.id)
	check_transaction_notify.delay(instance.id)
