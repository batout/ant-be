# Generated by Django 3.2.12 on 2022-08-15 17:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transactions", "0005_alter_transaction_date_of_transaction"),
    ]

    operations = [
        migrations.AlterField(
            model_name="transaction",
            name="title",
            field=models.CharField(max_length=250),
        ),
    ]
