# Generated by Django 3.2.12 on 2022-10-17 17:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ("transactions", "0010_auto_20221003_1635"),
    ]

    operations = [
        migrations.AlterField(
            model_name="transaction",
            name="notes",
            field=models.TextField(blank=True, default="", max_length=1000, null=True),
        ),
    ]
