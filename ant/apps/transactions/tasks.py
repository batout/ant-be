import time
import json
from django.utils.translation import ugettext_lazy as _

# use celery app
from config.celery_app import *

# use services
from ant.apps.transactions.api.v1.selectors import BudgetSelectors
from ant.apps.notifications.api.v1.services import NotificationServices

# models
from ant.apps.notifications.models import Notification, Navigation
from ant.apps.budgets.models import Budget
from .models import Transaction


@shared_task
def check_transaction_notify(transaction_id):
    transaction = Transaction.objects.filter(id=transaction_id).first()
    if transaction:
        budgets = Budget.objects.filter(user=transaction.user).all()
        n_controller = NotificationServices()
        if transaction.category is not None:
            for budget in budgets:
                if transaction.category in budget.categories.all():
                    transactions = Transaction.objects.filter(
                        user=transaction.user, category__in=budget.categories.all())
                    budget_services = BudgetSelectors(transactions)
                    expenses = budget_services.calculate_budget_expenses(budget.start_date, budget.end_date)
                    # print('expenses/budget.amount*100', str((expenses/budget.amount*100) >= 90))
                    # print('expenses', expenses)
                    # print('budget.amount', budget.amount)
                    # print('budget.amount*100', budget.amount*100)
                    if (expenses/budget.amount*100) >= 90:
                        navigation = Navigation.objects.create(
                            module_name='HsabatBudgetsAndGoalsDetailsScreen',
                            data=json.dumps({"budgetId": budget.id, "categoryId": transaction.category.id, }),
                            transaction_id=transaction.id,
                        )
                        n_controller.create_notification(Notification.NotificationType.SPECIFIC, _('You Exceed 90% of budget'), _(
                            'You Exceed 90% of budget'), user=transaction.user, navigation=navigation)
        else:
            # assuming obj is a model transaction
            navigation = Navigation.objects.create(
                module_name='EditTransactionScreen',
                transaction_id=transaction.id,
            )
            n_controller.create_notification(Notification.NotificationType.SPECIFIC, _('This transaction doesn\'t related to any category'), _(
                'This transaction doesn\'t related to any category'), user=transaction.user, navigation=navigation)
