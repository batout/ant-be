from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from ant.apps.accounts.models import Account
from ant.apps.categories.models import Category
from ant.apps.currencies.models import Currency
from ant.apps.tags.models import Tag
from ant.apps.users.models import User
from ant.utils.models import BaseModel


class Payee(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=50)
    logo = models.ImageField(null=True, blank=True)
    address = models.CharField(max_length=200, null=True, blank=True)


class TransactionManager(models.Manager):
    def get_queryset(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs).exclude(
            account__is_archived=True).filter(is_archived=False)

    def get_archived_transactions(self, *args, **kwargs):
        return super().get_queryset(*args, **kwargs)


class Transaction(BaseModel):
    objects = TransactionManager()

    class TransactionType(models.TextChoices):
        EXPENSE = "EXPENSE", _("Expense")
        INCOME = "INCOME", _("Income")

    title = models.CharField(max_length=250)
    notes = models.TextField(max_length=1000, blank=True, null=True, default="")
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    account = models.ForeignKey(Account, on_delete=models.CASCADE, null=True, related_name="transactions")
    transaction_type = models.CharField(choices=TransactionType.choices, max_length=10)
    date_of_transaction = models.DateField(null=True)
    timestamp = models.DateTimeField(null=True)
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True, related_name="transactions"
    )
    tags = models.ManyToManyField(Tag, blank=True)
    payee = models.ForeignKey(Payee, on_delete=models.SET_NULL, null=True, blank=True)
    amount = models.DecimalField(decimal_places=2, max_digits=14)
    currency = models.ForeignKey(
        Currency, on_delete=models.SET_NULL, null=True, blank=True
    )
    receipt = models.FileField(null=True, blank=True, default=None)
    lean_transaction_id = models.UUIDField(null=True, editable=False)
    is_archived = models.BooleanField(default=False)

    def __str__(self):
        return self.title if self.title is not None else "?"

    def save(self, *args, **kwargs):
        old = Transaction.objects.filter(id=self.id).first()
        self.update_wallet_balance(old)
        if self.currency is None:
            self.currency, created = Currency.objects.get_or_create(code="SAR")

        if self.date_of_transaction and not self.timestamp:
            self.timestamp = self.date_of_transaction
        super(Transaction, self).save(*args, **kwargs)

        if not self.timestamp:
            self.timestamp = self.added_at
            self.save()
        # notify user of exceeding the amount limit of budget

    def update_wallet_balance(self, old):
        # take care of transaction type
        if self.account != None:
            if self.account.account_type == Account.AccountType.CASH:
                if old is not None:
                    if old.amount != self.amount:
                        if self.transaction_type == Transaction.TransactionType.EXPENSE:
                            Account.objects.filter(id=self.account.id).update(
                                opening_balance=models.F("opening_balance")
                                + old.amount
                                - self.amount
                            )
                        else:
                            Account.objects.filter(id=self.account.id).update(
                                opening_balance=models.F("opening_balance")
                                - old.amount
                                + self.amount
                            )
                else:
                    if self.transaction_type == Transaction.TransactionType.EXPENSE:
                        Account.objects.filter(id=self.account.id).update(
                            opening_balance=models.F("opening_balance") - self.amount
                        )
                    else:
                        Account.objects.filter(id=self.account.id).update(
                            opening_balance=models.F("opening_balance") + self.amount
                        )

    def delete(self, *args, **kwargs):
        if self.account.account_type == Account.AccountType.CASH:
            if self.transaction_type == Transaction.TransactionType.EXPENSE:
                Account.objects.filter(id=self.account.id).update(
                    opening_balance=models.F("opening_balance") + self.amount
                )
            else:
                Account.objects.filter(id=self.account.id).update(
                    opening_balance=models.F("opening_balance") - self.amount
                )
        super(Transaction, self).delete(*args, **kwargs)


class PendingPayment(BaseModel):
    lean_payment_id = models.UUIDField(null=False)
    lean_customer_id = models.UUIDField(null=False)
    payment_status = models.CharField(max_length=50)
    check_transaction_trials = models.IntegerField(default=0)
    check_transaction_status = models.BooleanField(default=False)
