# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Payee, Transaction


@admin.register(Payee)
class PayeeAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "user",
        "name",
        "logo",
        "address",
    )
    list_filter = ("added_at", "updated_at", "user")
    search_fields = ("name",)
    date_hierarchy = "updated_at"


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "title",
        "notes",
        "user",
        "account",
        "transaction_type",
        "date_of_transaction",
        "category",
        "payee",
        "amount",
        "currency",
        "receipt",
    )
    list_filter = (
        "added_at",
        "updated_at",
        "user",
        "account",
        "date_of_transaction",
        "category",
        "payee",
        "currency",
    )
    raw_id_fields = ("tags",)
    date_hierarchy = "updated_at"

    def get_queryset(self, request):
        return (
            super(TransactionAdmin, self)
            .get_queryset(request)
            .prefetch_related("category", "payee", "currency")
        )
