from django.apps import AppConfig


class TransactionsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ant.apps.transactions"
    def ready(self):
        import ant.apps.transactions.signals