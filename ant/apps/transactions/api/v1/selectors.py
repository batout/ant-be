from datetime import datetime
from django.conf import settings
import pytz
from django.db.models import Sum


class BudgetSelectors():
    def __init__(self, transactions):
        self.transactions = transactions.filter(account__is_archived=False)
        
    def calculate_budget_expenses(self, start_date, end_date):
        if (
            start_date is not None
            and start_date != ""
            and end_date is not None
            and end_date != ""
        ):
            start_date = datetime.fromisoformat(str(start_date))
            end_date = datetime.fromisoformat(str(end_date))
            tz = pytz.timezone(settings.TIME_ZONE)
            start_date = start_date.replace(tzinfo=tz)
            end_date = end_date.replace(tzinfo=tz)
            self.transactions = self.transactions.filter(
                timestamp__gte=start_date, timestamp__lte=end_date
            )
        elif start_date and start_date != "":
            start_date = datetime.fromisoformat(str(start_date))
            tz = pytz.timezone(settings.TIME_ZONE)
            start_date = start_date.replace(tzinfo=tz)
            self.transactions = self.transactions.filter(timestamp__gte=start_date)
        elif end_date and end_date != "":
            end_date = datetime.fromisoformat(end_date)
            tz = pytz.timezone(settings.TIME_ZONE)
            end_date = end_date.replace(tzinfo=tz)
            self.transactions = self.transactions.filter(timestamp__lte=end_date)

        return self.transactions.aggregate(total=Sum("amount"))["total"] or 0
