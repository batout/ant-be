from rest_framework import serializers

from ant.apps.accounts.api.v1.serializers import AccountSerializer
from ant.apps.banks.api.v1.serializers import BankSerializer
from ant.apps.categories.api.v1.serializers import CategorySerializer
from ant.apps.currencies.api.v1.serializers import CurrencySerializer
from ant.apps.transactions.models import Payee, Transaction
from ant.utils.serializers import ModelSerializer


class PayeeSerializer(ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(), write_only=True
    )

    class Meta:
        model = Payee
        fields = [
            "id",
            "user",
            "name",
            "logo",
            "address",
        ]


class TransactionSerializer(ModelSerializer):
    user = serializers.HiddenField(
        default=serializers.CurrentUserDefault(), write_only=True
    )
    bank = BankSerializer(source="account.bank", read_only=True)

    class Meta:
        model = Transaction
        fields = [
            "id",
            "title",
            "notes",
            "user",
            "transaction_type",
            "date_of_transaction",
            "category",
            "timestamp",
            "tags",
            "payee",
            "amount",
            "account",
            "currency",
            "receipt",
            "bank",
            "is_archived",
        ]


class TransactionSerializerRead(TransactionSerializer):
    category = CategorySerializer(
        fields=[
            "title",
            "icon",
            "color",
            "category_type",
        ]
    )
    currency = CurrencySerializer()
    payee = PayeeSerializer(read_only=True)
    bank = BankSerializer(source="account.bank", read_only=True)
    account = AccountSerializer(read_only=True)
    receipt = serializers.FileField()
