from rest_framework import status
from rest_framework.test import APIClient, APITestCase

from ant.apps.accounts.models import Account
from ant.apps.categories.models import Category
from ant.apps.transactions.models import Transaction
from ant.apps.users.models import User


class CreateTransactionTest(APITestCase):
    @classmethod
    def setUpTestData(cls) -> None:
        cls.user = User.objects.create(phone_number="+966500000000")
        cls.account = Account.objects.create(
            user=cls.user, title="Test Account", account_type="CASH"
        )
        cls.category = Category.objects.create(user=cls.user, title="Test Category")
        cls.client = APIClient()
        cls.client.force_authenticate(user=cls.user)

    def setUp(self) -> None:
        self.client = APIClient()
        self.client.force_authenticate(user=CreateTransactionTest.user)

    def test_create_transaction(self):
        data = {
            "title": "Test Transaction",
            "account": self.account.id,
            "category": self.category.id,
            "transaction_type": "EXPENSE",
            "amount": 100,
        }
        response = self.client.post("/api/v1/transactions/", data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # test transaction timestamp
        transaction = Transaction.objects.get(id=response.data["id"])
        self.assertEqual(transaction.timestamp, transaction.added_at)
