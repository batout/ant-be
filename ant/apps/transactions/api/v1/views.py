from datetime import datetime
import pytz
from django.conf import settings
from django.db.models import Count, F, Q, Sum
from django.utils.translation import ugettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import filters, status, viewsets
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.generics import GenericAPIView
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response

from ant.apps.banks.api.v1.serializers import BankSerializer
from ant.apps.banks.models import Bank
from ant.apps.budgets.models import Budget
from ant.apps.categories.api.v1.serializers import CategorySerializer
from ant.apps.categories.models import Category
from ant.apps.goals.models import UserGoal
from ant.apps.transactions.api.v1.serializers import (
    PayeeSerializer,
    TransactionSerializer,
    TransactionSerializerRead,
)
from ant.apps.transactions.models import Payee, Transaction


class TransactionViewSet(viewsets.ModelViewSet):
    serializer_class = TransactionSerializer
    queryset = Transaction.objects.all()
    filter_backends = [DjangoFilterBackend, filters.OrderingFilter]
    ordering_fields = [
        "id",
        "timestamp",
        "title",
        "amount",
    ]

    filterset_fields = {
        "added_at": ["gte", "lte", "exact", "gt", "lt"],
        "timestamp": ["gte", "lte", "exact", "gt", "lt"],
        "date_of_transaction": ["gte", "lte", "exact", "gt", "lt"],
        "transaction_type": ["exact"],
        "category": ["exact"],
        "payee": ["exact"],
        "currency": ["exact"],
        "account": ["exact"],
        "is_archived": ["exact"],
        "account__is_archived": ["exact"]
    }

    def get_queryset(self):
        queryset = Transaction.objects.get_archived_transactions().filter(user=self.request.user).exclude(title="").exclude(title__isnull=True).select_related(
            "user", "account", "category", "payee", "currency", "user__city", "payee__user", "account__bank", "account__currency", "currency__country").prefetch_related("tags")
        is_archived_param = self.request.GET.get('account__is_archived', None)

        if is_archived_param == None:
            queryset = queryset.exclude(account__is_archived=True)
        return self.filter_queryset(queryset)
        # return queryset

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return TransactionSerializerRead
        return TransactionSerializer

    @action(detail=False, methods=["get"])
    def chart(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        filtered_queryset = self.filter_queryset(queryset)
        qs = (
            filtered_queryset.order_by("timestamp__date")
            .filter(timestamp__isnull=False)
            .values("timestamp__date")
            .annotate(sum=Sum("amount"))
            .annotate(month=F("timestamp__date"))
            .values("sum", "month")
        )

        return Response(qs, status=status.HTTP_200_OK)

    @action(methods=["get"], detail=False)
    def grouped_categories(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        filtered_queryset = self.filter_queryset(queryset)
        qs = filtered_queryset.values("category").annotate(
            transactions_count=Count("id"), transactions_sum=Sum("amount")
        )
        data = []
        for element in qs:
            banks = Bank.objects.filter(
                id__in=filtered_queryset.values_list("account__bank_id", flat=True)
            )
            category = Category.objects.filter(id=element["category"]).first()
            if category:
                data.append(
                    {
                        "category": CategorySerializer(
                            category,
                            context=self.get_serializer_context(),
                        ).data,
                        "transactions_count": element["transactions_count"],
                        "transactions_sum": element["transactions_sum"],
                        "banks": BankSerializer(banks, many=True).data,
                    }
                )

        return Response(data, status=status.HTTP_200_OK)

    @action(methods=["get"], detail=False)
    def incomes_expenses_per_month(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        filtered_queryset = self.filter_queryset(queryset)
        qs = (
            filtered_queryset.filter(timestamp__isnull=False)
            .values("transaction_type", "timestamp__date")
            .annotate(transactions_sum=Sum("amount"))
            .order_by("timestamp__date")
        )

        results = {}
        for element in qs:
            date = element["timestamp__date"].strftime("%Y-%m-%d")
            if date not in results:
                results[date] = {"incomes": 0, "expenses": 0, "date": date}
            if element["transaction_type"] == Transaction.TransactionType.INCOME:
                results[date]["incomes"] = element["transactions_sum"]
            elif element["transaction_type"] == Transaction.TransactionType.EXPENSE:
                results[date]["expenses"] = abs(element["transactions_sum"])

        return Response(results.values(), status=status.HTTP_200_OK)

    @action(methods=["get"], detail=False)
    def gross_income(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        filtered_queryset = self.filter_queryset(queryset)

        user = self.request.user
        actual_income = filtered_queryset.filter(
            user=user,
            transaction_type=Transaction.TransactionType.INCOME,
        ).aggregate(actual_income=Sum("amount"))["actual_income"]
        if actual_income is None:
            actual_income = 0

        expected_income = 0
        if user.monthly_salary:
            expected_income += user.monthly_salary

        if user.monthly_real_estate_income:
            expected_income += user.monthly_real_estate_income

        if user.monthly_stock_income:
            expected_income += user.monthly_stock_income

        if user.monthly_other_income:
            expected_income += user.monthly_other_income

        if not expected_income:
            raise NotFound(_("User didn't enter any incomes"))
        return Response(
            {"expected_income": expected_income, "actual_income": actual_income},
            status=status.HTTP_200_OK,
        )


class MonthGoalsGrouped(GenericAPIView):
    permission_classes = [IsAuthenticated]

    def get(self, request, *args, **kwarg):
        user = self.request.user
        start_date = self.request.query_params.get("start_date", None)
        if start_date is not None and start_date != "":
            start_date = datetime.fromisoformat(start_date)
            tz = pytz.timezone(settings.TIME_ZONE)
            start_date = start_date.replace(tzinfo=tz)
        end_date = self.request.query_params.get("end_date", None)

        if end_date is not None and end_date != "":
            end_date = datetime.fromisoformat(end_date)
            tz = pytz.timezone(settings.TIME_ZONE)
            end_date = end_date.replace(tzinfo=tz)

        income_target = user.get_expected_monthly_income()
        income_real = Transaction.objects.filter(
            user=user, transaction_type=Transaction.TransactionType.INCOME
        )
        goals = UserGoal.objects.filter(user=user)
        budget_target = (
            Budget.objects.filter(user=user).aggregate(target=Sum("amount"))["target"]
            or 0
        )
        budget_real = Transaction.objects.filter(
            category__in=Budget.objects.filter(user=user).values_list("categories"),
            user=user,
        )

        goals_values = 0
        for goal in goals:
            if end_date is not None and end_date != "":
                goals_values += goal.reached_value(date=end_date)
            else:
                goals_values += goal.reached_value()

        if start_date and start_date != "":
            income_real = income_real.filter(timestamp__gte=start_date)
            budget_real = budget_real.filter(timestamp__gte=start_date)

        if end_date and end_date != "":
            income_real = income_real.filter(
                timestamp__lte=end_date,
            )
            budget_real = budget_real.filter(timestamp__lte=end_date)

        return Response(
            {
                "income_target": income_target,
                "income_real": income_real.aggregate(income_real=Sum("amount"))[
                    "income_real"
                ]
                or 0,
                "goals_values": goals_values,
                "budget_target": budget_target,
                "budget_real": budget_real.aggregate(budget_real=Sum("amount"))[
                    "budget_real"
                ]
                or 0,
            }
        )


class PayeeViewSet(viewsets.ModelViewSet):
    serializer_class = PayeeSerializer
    queryset = Payee.objects.all()

    def get_queryset(self):
        user = self.request.user
        return (
            super(PayeeViewSet, self)
            .get_queryset()
            .filter(Q(user__isnull=True) | Q(user=user))
        )
