from django.urls import path
from rest_framework import routers

from ant.apps.transactions.api.v1.views import (
    MonthGoalsGrouped,
    PayeeViewSet,
    TransactionViewSet,
)

app_name = "transactions"
router = routers.DefaultRouter()

router.register("transactions", TransactionViewSet)
router.register("payees", PayeeViewSet)
urlpatterns = router.urls + [path("month_goals_grouped/", MonthGoalsGrouped.as_view())]
