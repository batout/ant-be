from django.contrib.auth import get_user_model

from config import celery_app
from config.celery_app import *
import requests
import json

from .models import VerificationCode

User = get_user_model()


@celery_app.task()
def get_users_count():
    """A pointless Celery task to demonstrate usage."""
    return User.objects.count()

@shared_task
def send_sms_notify(verification_code_id):
    verification_code = VerificationCode.objects.get(verification_code_id=verification_code_id)
    url = "https://el.cloud.unifonic.com/rest/SMS/messages"
    body = f"""
    أهلا بك رمز التفعيل: {verification_code.code}
    """
    payload = {
        "AppSid": "UzcXcE6UxUa70l6yx1ewfPz1vCSQTk",
        "Recipient": str(verification_code.phone_number),
        "Body": body,
        "responseType": "JSON",
        "baseEncode": "true",
        "statusCallback": "sent",
        "async": "true",
    }
    headers = {"Accept": "application/json"}
    response = requests.request("POST", url, headers=headers, data=payload)
    if (
        response.status_code != 200
        and json.loads(response.text)["message"] != "SUCCESS"
    ):
        send_sms_notify(verification_code_id)
