from django.contrib.auth import get_user_model, password_validation
from django.utils.translation import ugettext_lazy as _
from phonenumber_field.serializerfields import PhoneNumberField
from rest_framework import serializers
from django.db.models import Q

from ant.apps.users.models import City
from ant.utils.serializers import ModelSerializer

User = get_user_model()


class EmptySerializer(serializers.Serializer):
    pass


class LoginInputSerializer(serializers.Serializer):
    phone_number = PhoneNumberField(required=True, trim_whitespace=True)


class PasswordChangeSerializer(serializers.Serializer):
    current_password = serializers.CharField(required=True)
    new_password = serializers.CharField(required=True)

    def validate_current_password(self, value):
        if not self.context["request"].user.check_password(value):
            raise serializers.ValidationError(_("Current password does not match"))
        return value

    def validate_new_password(self, value):
        password_validation.validate_password(value)
        return value


class PhoneNumberConfirmationSerializer(serializers.Serializer):
    phone_number = PhoneNumberField(required=True, trim_whitespace=True)
    code = serializers.CharField(max_length=6)


class PasswordSetSerializer(serializers.Serializer):
    new_password = serializers.CharField(required=True)

    def validate_new_password(self, value):
        password_validation.validate_password(value)
        return value


class UserLoginSerializer(ModelSerializer):
    class Meta:
        model = User
        fields = [
            "id",
            "name",
            "email",
            "phone_number",
            "date_of_birth",
            "gender",
            "is_active",
            "date_joined",
            "has_usable_password",
            "lean_customer_id",
        ]


class ResendVerificationSerializer(serializers.Serializer):
    phone_number = PhoneNumberField(required=True, trim_whitespace=True)


class UserSerializer(ModelSerializer):
    email = serializers.EmailField()

    class Meta:
        model = User
        fields = [
            "id",
            "avg_monthly_income",
            "email",
            "name",
            "age",
            "city",
            "gender",
            "date_of_birth",
            "monthly_salary",
            "monthly_real_estate_income",
            "monthly_stock_income",
            "monthly_other_income",
            "notification_active",
            "fcm_token",
        ]

    def validate_email(self, value):
        lower_email = value.lower()
        if User.objects.filter(~Q(id=self.context['request'].user.id), email__iexact=lower_email).exists():
            raise serializers.ValidationError("Duplicate")
        return lower_email


class CheckUserPasswordSerializer(serializers.Serializer):
    phone_number = PhoneNumberField(required=True, trim_whitespace=True)
    password = serializers.CharField(required=True)


class CheckUserPasswordForResetSerializer(serializers.Serializer):
    password = serializers.CharField(required=True)


class CitySerializer(serializers.ModelSerializer):
    class Meta:
        model = City
        fields = [
            "id",
            "name",
        ]


class ActivateNotificationSerializer(serializers.Serializer):
    notification_active = serializers.BooleanField(required=True)


class FCMNotificationSerializer(serializers.Serializer):
    fcm_token = serializers.CharField(required=True)
