from django.contrib.auth import get_user_model, logout
from django.core.exceptions import ImproperlyConfigured
from django.utils.translation import ugettext_lazy as _
from rest_framework import status, viewsets
from rest_framework.decorators import action
# from rest_framework_api_key.permissions import HasAPIKey
from rest_framework.response import Response
from rest_framework_simplejwt.tokens import RefreshToken
from rest_framework.authtoken.models import Token
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.authentication import TokenAuthentication


from django.conf import settings

from ...models import City, VerificationCode
from .serializers import (
    CheckUserPasswordSerializer,
    CitySerializer,
    EmptySerializer,
    LoginInputSerializer,
    PasswordChangeSerializer,
    CheckUserPasswordForResetSerializer,
    PasswordSetSerializer,
    PhoneNumberConfirmationSerializer,
    ResendVerificationSerializer,
    UserLoginSerializer,
    ActivateNotificationSerializer,
    UserSerializer,
)

User = get_user_model()


class AuthViewSet(viewsets.GenericViewSet):
    permission_classes = [
        AllowAny,
        # HasAPIKey
    ]
    serializer_class = EmptySerializer

    serializer_classes = {
        "login": LoginInputSerializer,
        "password_change": PasswordChangeSerializer,
        "phone_number_confirmation": PhoneNumberConfirmationSerializer,
        "set_password": PasswordSetSerializer,
        "resend_verification_code": ResendVerificationSerializer,
        "check_user_password": CheckUserPasswordSerializer,
        "activate_notification": ActivateNotificationSerializer,
    }

    @action(
        methods=[
            "post",
        ],
        detail=False,
        serializer_class=LoginInputSerializer,
    )
    def login(self, request):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        if not settings.STAGE_ENABLED:
            phone_number = serializer.validated_data["phone_number"]
            VerificationCode.objects.create(phone_number=phone_number)
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Sms verification code has been sent successfully."),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=[
            "POST",
        ],
        detail=False,
    )
    def logout(self, request):
        logout(request)

        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Successfully logged out"),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["POST"],
        detail=False,
        permission_classes=[
            IsAuthenticated,
        ],
        serializer_class=PasswordChangeSerializer,
    )
    def password_change(self, request):

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        request.user.set_password(serializer.validated_data["new_password"])
        request.user.force_password_reset = False
        request.user.save()
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("password changed successfully"),
            },
            status=status.HTTP_200_OK,
        )

    def get_serializer_class(self):
        if not isinstance(self.serializer_classes, dict):
            raise ImproperlyConfigured("serializer_classes should be a dict mapping.")

        if self.action in self.serializer_classes.keys():
            return self.serializer_classes[self.action]
        return super().get_serializer_class()

    @action(
        methods=["POST"],
        detail=False,
        permission_classes=[
            AllowAny,
            # HasAPIKey
        ],
        serializer_class=PhoneNumberConfirmationSerializer,
    )
    def phone_number_confirmation(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = serializer.validated_data.get("phone_number")
        code = serializer.validated_data.get("code")
        if not settings.STAGE_ENABLED:  # add new condition of check if the system in stage
            if not VerificationCode.objects.filter(
                phone_number=phone_number, code=code, is_active=True
            ).exists():
                return Response(
                    data={
                        "data": {},
                        "status": 0,
                        "status_code": status.HTTP_400_BAD_REQUEST,
                        "message": _("Wrong Verification code."),
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
            else:
                VerificationCode.objects.filter(
                    phone_number=phone_number, code=code, is_active=True
                ).update(is_active=False)
        else:
            if code != settings.STAGE_VERIFICATION_CODE:
                return Response(
                    data={
                        "data": {},
                        "status": 0,
                        "status_code": status.HTTP_400_BAD_REQUEST,
                        "message": _("Wrong Verification code."),
                    },
                    status=status.HTTP_400_BAD_REQUEST,
                )
        user, created = User.objects.get_or_create(phone_number=phone_number)
        if created:
            user.set_unusable_password()
            user.save()
        # refresh = RefreshToken.for_user(user)
        Token.objects.filter(user=user).delete()
        token = Token.objects.create(user=user)
        return Response(
            data={
                "data": {
                    "token": {
                        # "refresh": str(refresh),
                        # "access": str(refresh.access_token),
                        "access": str(token.key)
                    },
                    "user": UserLoginSerializer(user).data,
                },
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Successfully logged in."),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["post"],
        detail=False,
        authentication_classes=[TokenAuthentication],
        permission_classes=[IsAuthenticated],
        # serializer_class=PasswordSetSerializer,
    )
    def set_password(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.request.user
        user.set_password(serializer.validated_data.get("new_password"))
        user.save()
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Password has been set successfully."),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["post"],
        detail=False,
        permission_classes=[],
        serializer_class=ResendVerificationSerializer,
    )
    def resend_verification_code(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = serializer.validated_data.get("phone_number", None)
        VerificationCode.objects.create(phone_number=phone_number)
        # verification_code.fire()
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Sms verification code has been resent successfully."),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["post"],
        detail=False,
        # authentication_classes=[TokenAuthentication],
        # permission_classes=[IsAuthenticated],
        serializer_class=CheckUserPasswordSerializer,
    )
    def check_user_password(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        phone_number = serializer.validated_data.get("phone_number", None)
        password = serializer.validated_data.get("password", None)
        refresh = RefreshToken.for_user(user)
        if str(user.phone_number) == phone_number and user.check_password(password):
            Token.objects.filter(user=user).delete()
            return Response(
                data={
                    "data": {
                        "token": {
                            "refresh": str(refresh),
                            "access": str(refresh.access_token),
                        },
                    },
                    "status": 1,
                    "status_code": status.HTTP_200_OK,
                    "message": _("User credentials is correct."),
                },
                status=status.HTTP_200_OK,
            )
        return Response(
            data={
                "data": {},
                "status": 0,
                "status_code": status.HTTP_401_UNAUTHORIZED,
                "message": _("User credentials is incorrect."),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["POST"],
        detail=False,
        permission_classes=[
            IsAuthenticated,
        ],
        serializer_class=CheckUserPasswordForResetSerializer,
    )
    def check_user_password_for_reset(self, request, *args, **kwargs):
        user = self.request.user
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        password = serializer.validated_data.get("password", None)
        if user.check_password(password):
            return Response(
                data={
                    "data": {},
                    "status": 1,
                    "status_code": status.HTTP_200_OK,
                    "message": _("User password is correct."),
                },
                status=status.HTTP_200_OK,
            )
        return Response(
            data={
                "data": {},
                "status": 0,
                "status_code": status.HTTP_400_BAD_REQUEST,
                "message": _("incorrect password"),
            },
            status=status.HTTP_200_OK,
        )

    @action(
        methods=["post"],
        detail=False,
        permission_classes=[IsAuthenticated],
        serializer_class=CheckUserPasswordSerializer,
    )
    def activate_notification(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = self.request.user
        notification_active = serializer.validated_data.get("notification_active", True)
        user.notification_active = notification_active
        user.save()
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Notification activated successfully."),
            },
            status=status.HTTP_200_OK,
        )


class UserViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = UserSerializer
    queryset = User.objects.all()

    def get_queryset(self):
        return super(UserViewSet, self).get_queryset().filter(id=self.request.user.id)

    @action(methods=["patch"], detail=False, url_path=r"", url_name="")
    def avg_monthly_income(self, *args, **kwargs):
        user = self.request.user
        serializer = UserSerializer(
            data=self.request.data, fields=["avg_monthly_income",
                                            "monthly_salary",
                                            "monthly_real_estate_income",
                                            "monthly_stock_income",
                                            "monthly_other_income"], partial=True)
        serializer.is_valid(raise_exception=True)
        serializer.update(user, validated_data=serializer.validated_data)
        return Response(
            data={
                "data": serializer.data,
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Successfully changed avg monthly income."),
            },
            status=status.HTTP_200_OK,
        )

    @action(methods=["patch"], detail=False, url_path=r"", url_name="")
    def complete_user_data(self, request, *args, **kwargs):
        user = self.request.user
        serializer = UserSerializer(
            data=self.request.data,
            context={'request': self.request},
            fields=[
                "name",
                "age",
                "email",
                "city",
                "gender",
                "date_of_birth",
                "monthly_salary",
                "monthly_real_estate_income",
                "monthly_stock_income",
                "monthly_other_income",
                "notification_active",
                "fcm_token",
            ],
            partial=True,
        )
        serializer.is_valid(raise_exception=True)
        serializer.update(user, validated_data=serializer.validated_data)

        return Response(
            data={
                "data": request.data,
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Successfully completed user profile data."),
            },
            status=status.HTTP_200_OK,
        )


class CityViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = CitySerializer
    queryset = City.objects.all()
