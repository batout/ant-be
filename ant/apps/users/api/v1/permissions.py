from django.utils.translation import gettext as _
from rest_framework import permissions
from rest_framework.request import Request
from rest_framework.views import APIView


class HasNotUsablePassword(permissions.BasePermission):
    message = _("User already have password.")

    def has_permission(self, request: Request, view: APIView) -> bool:
        return not request.user.has_usable_password()
