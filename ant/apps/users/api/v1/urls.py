from rest_framework import routers
from django.urls import path

from ant.apps.users.api.v1.views import AuthViewSet, CityViewSet, UserViewSet

app_name = "users"

router = routers.DefaultRouter()

router.register("auth", AuthViewSet, basename="auth")
router.register("users", UserViewSet, basename="users")
router.register("cities", CityViewSet, basename="cities")

urlpatterns = router.urls
