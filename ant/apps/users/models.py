import json

import requests
from django.conf import settings
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models import JSONField
from django.urls import reverse
from django.utils.crypto import get_random_string
from django.utils.translation import gettext_lazy as _
from django.contrib.auth.base_user import BaseUserManager
from phonenumber_field.modelfields import PhoneNumberField

from ant.apps.banks.models import Bank
from ant.utils.models import BaseModel


class UserManager(BaseUserManager):
    use_in_migrations = True

    def _create_user(self, email, password, phone_number, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, username=phone_number, phone_number=phone_number, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, phone_number, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')
        return self._create_user(email, password, phone_number, **extra_fields)


class User(AbstractUser, BaseModel):
    """
    Default custom user model for ant.
    If adding fields that need to be filled at user signup,
    check forms.SignupForm and forms.SocialSignupForms accordingly.
    """
    objects = UserManager()

    class Gender(models.TextChoices):
        Male = "M", _("Male")
        Female = "F", _("Female")

    #: First and last name do not cover name patterns around the globe
    name = models.CharField(_("Name of User"), blank=True, max_length=255)
    first_name = None  # type: ignore
    last_name = None  # type: ignore
    email = models.EmailField(_("email address"), unique=True, null=True)
    phone_number = PhoneNumberField(unique=True)
    date_of_birth = models.DateField(null=True, blank=True)
    gender = models.CharField(
        max_length=1, choices=Gender.choices, null=True, blank=True, default=None
    )
    username = models.CharField(max_length=100, null=True, blank=True, default=None)  # type:ignore
    avg_monthly_income = models.DecimalField(
        decimal_places=2, max_digits=20, default="0.0"
    )
    age = models.PositiveSmallIntegerField(default=None, null=True)
    lean_customer_id = models.UUIDField(null=True)
    city = models.ForeignKey("City", on_delete=models.SET_NULL, null=True)
    monthly_salary = models.DecimalField(
        decimal_places=2, max_digits=20, null=True, default=None
    )
    monthly_real_estate_income = models.DecimalField(
        decimal_places=2, max_digits=20, null=True, default=None
    )
    monthly_stock_income = models.DecimalField(
        decimal_places=2, max_digits=20, null=True, default=None
    )
    monthly_other_income = models.DecimalField(
        decimal_places=2, max_digits=20, null=True, default=None
    )
    notification_active = models.BooleanField(default=True)
    # FCM token
    fcm_token = models.CharField(_("fcm token"), blank=True, max_length=255)
    USERNAME_FIELD = "phone_number"
    REQUIRED_FIELDS = ["email"]

    def get_absolute_url(self):
        """Get url for user's detail view.

        Returns:
            str: URL for user detail.

        """
        return reverse("users:detail", kwargs={"username": self.id})

    def __str__(self):
        return "".join(str(self.phone_number))

    def save(self, *args, **kwargs):
        super(User, self).save(*args, **kwargs)
        if self.lean_customer_id is None:
            # return
            self.create_lean_customer()

    def create_lean_customer(self):
        url = settings.LEAN_BASE_URL + "customers/v1"
        payload = json.dumps(
            {"app_user_id": f"{settings.LEAN_CUSTOMER_PREFIX}_{self.id}"}
        )
        headers = {
            "lean-app-token": settings.LEAN_AUTHENTICATION_TOKEN,
            "Content-Type": "application/json",
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        if response.status_code == 200:
            self.lean_customer_id = response.json()["customer_id"]
            self.save()

    def get_expected_monthly_income(self):
        return (
            (self.monthly_salary or 0)
            + (self.monthly_real_estate_income or 0)
            + (self.monthly_stock_income or 0)
            + (self.monthly_other_income or 0)
        )


class UserEntity(BaseModel):
    bank = models.ForeignKey(Bank, on_delete=models.CASCADE, related_name="entities")
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="entities")
    entity_id = models.UUIDField()
    identity = JSONField(null=True)

    def save(self, *args, **kwargs):
        if self.identity is None:
            self.identity = self.get_lean_identity()
        super(UserEntity, self).save(*args, **kwargs)

    def get_lean_identity(self):
        import json

        import requests

        url = f"{settings.LEAN_BASE_URL}data/v1/identity"

        payload = json.dumps({"entity_id": self.entity_id})
        headers = {
            "Content-Type": "application/json",
            "lean-app-token": settings.LEAN_AUTHENTICATION_TOKEN,
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        return response.json().get("payload", None)

    def get_lean_accounts(self):
        import json

        import requests

        url = f"{settings.LEAN_BASE_URL}data/v1/accounts"

        payload = json.dumps({"entity_id": self.entity_id})
        headers = {
            "Content-Type": "application/json",
            "lean-app-token": settings.LEAN_AUTHENTICATION_TOKEN,
        }

        response = requests.request("POST", url, headers=headers, data=payload)

        return response.json().get("payload", {}).get("accounts", [])


class VerificationCode(BaseModel):
    phone_number = PhoneNumberField()
    code = models.CharField(max_length=6)
    is_active = models.BooleanField(default=True)

    def save(self, *args, **kwargs):
        if self.code in [None, ""]:
            self.code = get_random_string(length=6, allowed_chars="123456789")
        super(VerificationCode, self).save(*args, **kwargs)

    def __str__(self):
        return " ".join([str(self.phone_number), self.code])

    def fire(self):
        import requests

        url = "https://el.cloud.unifonic.com/rest/SMS/messages"
        body = f"""
        أهلا بك رمز التفعيل: {self.code}
        """
        payload = {
            "AppSid": "UzcXcE6UxUa70l6yx1ewfPz1vCSQTk",
            "Recipient": str(self.phone_number),
            "Body": body,
            "responseType": "JSON",
            "baseEncode": "true",
            "statusCallback": "sent",
            "async": "true",
        }
        headers = {"Accept": "application/json"}
        response = requests.request("POST", url, headers=headers, data=payload)
        if (
            response.status_code != 200
            and json.loads(response.text)["message"] != "SUCCESS"
        ):
            self.fire()


class Country(BaseModel):
    name = models.CharField(max_length=15)
    flag = models.ImageField()

    def __str__(self):
        return self.name


class City(BaseModel):
    country = models.ForeignKey(Country, on_delete=models.CASCADE)
    name = models.CharField(max_length=20)

    def __str__(self):
        return self.name
