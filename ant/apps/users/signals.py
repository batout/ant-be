# code
from django.db.models.signals import post_save
from django.dispatch import receiver
from .models import VerificationCode
from .tasks import send_sms_notify

@receiver(post_save, sender=VerificationCode)
def create_verification_code(sender, instance, created, **kwargs):
	send_sms_notify.delay(instance.id)

