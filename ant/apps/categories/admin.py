# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Category


@admin.register(Category)
class CategoryAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "parent",
        "user",
        "title",
        "icon",
        "color",
        "category_type",
    )
    list_filter = ("added_at", "updated_at", "parent", "user")
    date_hierarchy = "updated_at"
