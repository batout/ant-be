from rest_framework import serializers

from ant.apps.categories.models import Category
from ant.utils.serializers import ModelSerializer


class CategorySerializer(ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())

    class Meta:
        model = Category
        fields = [
            "id",
            "user",
            "title",
            "icon",
            "color",
            "category_type",
            "parent",
        ]
