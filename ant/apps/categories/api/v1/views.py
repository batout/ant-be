from django.db.models import Q
from django.utils.translation import ugettext_lazy as _
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.decorators import action
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

from ant.apps.categories.api.v1.serializers import CategorySerializer
from ant.apps.categories.models import Category
from ant.apps.budgets.models import Budget


class CategoryViewSet(viewsets.ModelViewSet):
    serializer_class = CategorySerializer
    queryset = Category.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = [
        "parent",
        "category_type",
    ]

    def get_queryset(self):
        user_budgets = Budget.objects.filter(user=self.request.user).all()
        all = self.request.GET.get('all','false')
        qs = super(CategoryViewSet, self).get_queryset().filter(Q(user__isnull=True) | Q(user=self.request.user)).exclude(title="").exclude(title__isnull=True)
        if all=='false':
            return (
                qs.exclude(budget__in=user_budgets)
            )
        return qs
    

    @action(
        methods=["get"], 
        permission_classes=[IsAuthenticated], 
        detail=False,
    )
    def get_all_categories(self, request, *args, **kwargs):
        category_type = request.GET.get('category_type','INCOME')
        qs = super(CategoryViewSet, self).get_queryset().filter(category_type=category_type).filter(Q(user__isnull=True) | Q(user=self.request.user)).exclude(title="").exclude(title__isnull=True)
        serializer = self.get_serializer(qs.all(), many=True)
        return Response(
            data={
                "data": serializer.data,
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Successfully changed avg monthly income."),
            },
            status=status.HTTP_200_OK,
        )
