from rest_framework import routers

from ant.apps.categories.api.v1.views import CategoryViewSet

app_name = "categories"

router = routers.DefaultRouter()

router.register("categories", CategoryViewSet)

urlpatterns = router.urls
