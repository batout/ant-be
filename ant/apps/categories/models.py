from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from ant.apps.users.models import User
from ant.utils.models import BaseModel


class Category(BaseModel):
    class CategoryType(models.TextChoices):
        EXPENSE = "EXPENSE", _("Expense")
        INCOME = "INCOME", _("Income")

    parent = models.ForeignKey("self", on_delete=models.CASCADE, null=True, blank=True)
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    title = models.CharField(max_length=100)
    icon = models.ImageField(null=True)
    color = models.CharField(max_length=7, null=True)
    category_type = models.CharField(
        choices=CategoryType.choices, max_length=10, null=True
    )
    lean_category_name = models.CharField(max_length=50, null=True, unique=True)

    def __str__(self):
        return self.title if self.title is not None else "?"
