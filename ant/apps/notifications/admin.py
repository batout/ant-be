# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Notification, UserNotification, Navigation



@admin.register(Notification)
class NotificationAdmin(admin.ModelAdmin):
    list_display = ("id", "updated_at", "body", "body_ar", "image", "title", "title_ar", "navigation")
    list_filter = ("updated_at",)
    search_fields = ("title","title_ar")
    date_hierarchy = "updated_at"

@admin.register(UserNotification)
class UserNotificationAdmin(admin.ModelAdmin):
    list_display = (
        "user",
        "notification",
        "is_read",
        "is_received",
    )
    list_filter = ("added_at", "updated_at", "user")
    date_hierarchy = "updated_at"


@admin.register(Navigation)
class NavigationAdmin(admin.ModelAdmin):
    list_display = (
        "module_name",
        "data",
    )
    list_filter = ("added_at", "updated_at")
    date_hierarchy = "updated_at"



