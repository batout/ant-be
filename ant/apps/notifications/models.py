from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from ant.apps.users.models import User
from ant.utils.models import BaseModel
from ant.apps.transactions.models import Transaction
class Notification(BaseModel):
    class NotificationType(models.TextChoices):
        SPECIFIC = "SPECIFIC", _("Specific")
        GENERAL = "GENERAL", _("General")
    title = models.CharField(max_length=100)
    title_ar = models.CharField(max_length=100)
    body = models.TextField()
    body_ar = models.TextField()
    image = models.ImageField(blank=True, null=True)
    navigation = models.ForeignKey('Navigation', on_delete=models.CASCADE, null=True, related_name='navigations')
    notification_type = models.CharField(
        max_length=20, choices=NotificationType.choices, null=True, blank=True, default=None)

class UserNotification(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    notification = models.ForeignKey(Notification, on_delete=models.CASCADE, related_name='notifications')
    is_read = models.BooleanField(default=False)
    is_received = models.BooleanField(default=False)

class Navigation(BaseModel):
    module_name =  models.CharField(max_length=255)
    data = models.TextField(default="{}")
    transaction = models.ForeignKey(Transaction, on_delete=models.CASCADE, null=True)
    
