from datetime import datetime

import pytz
import json
from django.conf import settings
from rest_framework import serializers

# models 
from ant.apps.notifications.models import Notification, UserNotification, Navigation

# serializers
from ant.utils.serializers import ModelSerializer
from ant.apps.transactions.api.v1.serializers import TransactionSerializerRead

class NavigationSerializer(ModelSerializer):
    data = serializers.SerializerMethodField()
    def get_data(self, obj):
        data = json.loads(obj.data)
        if data == {}:
            return TransactionSerializerRead(obj.transaction).data
        return data
    class Meta:
        model = Navigation
        fields = [
            "module_name",
            "data",
        ]
    
    
class NotificationSerializer(ModelSerializer):
    navigation = NavigationSerializer(read_only=True)
    class Meta:
        model = Notification
        fields = [
            "id",
            "title",
            "title_ar",
            "body",
            "body_ar",
            "navigation",
        ]


class UserNotificationSerializer(ModelSerializer):
    notification = serializers.JSONField(write_only=True)
    class Meta:
        model = UserNotification
        fields = [
            "id",
            "user",
            "notification",
            "is_read",
            "is_received",
        ]

    def create(self, validated_data):
        notification = validated_data.pop("notification", None)
        if isinstance(notification, int):
            new_notification = Notification.objects.get(id=notification)
        elif isinstance(notification, dict):
            new_notification = Notification.objects.create(**notification)
        else:
            new_notification = None
        validated_data["notification"] = new_notification
        return super(UserNotificationSerializer, self).create(validated_data)


class UserNotificationDetailSerializer(UserNotificationSerializer):
    notification = NotificationSerializer()

class ReadNotificationSerializer(serializers.Serializer):
    user_notification_id = serializers.PrimaryKeyRelatedField(queryset=UserNotification.objects.all())

