from django.conf import settings

from ant.apps.notifications.models import Notification , UserNotification
import requests
import json

class NotificationServices():
    def create_notification(self, notification_type, title, body, user, **kwargs):
        notification =  Notification.objects.create(title=title, body=body, **kwargs)
        if notification_type == Notification.NotificationType.SPECIFIC : 
            notification_to_send = UserNotification.objects.create(user=user, notification=notification)
            self.send_notification(notification_to_send)
    
    def send_notification(self, notification):
        url = settings.FCM_URL
        payload = json.dumps({
            "to": "{notification.user.fcm_token}",
            "notification": {
                "body": "{notification.notification.body}",
                "content_available": True,
                "priority": "high",
                "subtitle": "{notification.notification.title}",
                "Title": "{notification.notification.title}"
            },
            "data": {
                "priority": "high",
                "sound": "app_sound.wav",
                "content_available": True,
                "bodyText": "{notification.notification.body}",
                "organization": "ant-app"
            }
        })
        headers = {
            'Authorization': 'key={settings.FCM_SERVER_KEY}',
            'Content-Type': 'application/json'
        }
        response = requests.request("POST", url, headers=headers, data=payload)
