from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.exceptions import NotFound
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from django.utils.translation import ugettext_lazy as _

from ant.apps.notifications.api.v1.serializers import (
    NotificationSerializer,
    UserNotificationSerializer,
    UserNotificationDetailSerializer,
    ReadNotificationSerializer
)
from ant.apps.notifications.models import Notification, UserNotification


class NotificationViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = NotificationSerializer
    queryset = Notification.objects.all()


class UserNotificationViewSet(viewsets.ModelViewSet):
    serializer_class = UserNotificationSerializer
    queryset = UserNotification.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = {
        "notification": ["exact"],
    }

    def get_queryset(self):
        return (
            super(UserNotificationViewSet, self).get_queryset().filter(user=self.request.user).select_related("notification", "notification__navigation")
        )

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return UserNotificationDetailSerializer
        return super(UserNotificationViewSet, self).get_serializer_class()

    @action(
        methods=["post"], 
        permission_classes=[IsAuthenticated], 
        serializer_class=ReadNotificationSerializer,
        detail=False
    )
    def read(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user_notification_id = serializer.validated_data.get("user_notification_id", None)
        user_notification = UserNotification.objects.get(pk=user_notification_id.id)
        user_notification.is_read = True
        user_notification.save()
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Notification read successfully"),
            },
            status=status.HTTP_200_OK,
        )
    
    @action(methods=["post"], permission_classes=[IsAuthenticated], detail=False)
    def read_all(self, request, *args, **kwargs):
        UserNotification.objects.filter(user=self.request.user).update(is_read=True)
        return Response(
            data={
                "data": {},
                "status": 1,
                "status_code": status.HTTP_200_OK,
                "message": _("Notification read successfully"),
            },
            status=status.HTTP_200_OK,
        )
