from rest_framework import routers

from ant.apps.notifications.api.v1.views import NotificationViewSet, UserNotificationViewSet

router = routers.DefaultRouter()

router.register("notifications", NotificationViewSet)
router.register("user_notifications", UserNotificationViewSet)

app_name = "notifications"

urlpatterns = router.urls
