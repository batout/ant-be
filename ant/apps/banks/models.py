import requests
from django.conf import settings
from django.db import models

# Create your models here.
from ant.utils.models import BaseModel


class Bank(BaseModel):
    # lean_id = models.IntegerField(unique=True)
    identifier = models.CharField(max_length=100)

    name = models.CharField(max_length=100)
    logo = models.URLField()
    country_code = models.CharField(max_length=4)
    active = models.BooleanField(default=True)
    main_color = models.CharField(max_length=7)
    background_color = models.CharField(max_length=7)

    def __str__(self):
        return self.name
