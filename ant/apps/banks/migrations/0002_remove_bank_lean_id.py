# Generated by Django 3.2.12 on 2022-07-25 18:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ("banks", "0001_initial"),
    ]

    operations = [
        migrations.RemoveField(
            model_name="bank",
            name="lean_id",
        ),
    ]
