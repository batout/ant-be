# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Bank


@admin.register(Bank)
class BankAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "identifier",
        "name",
        "logo",
        "country_code",
        "active",
        "main_color",
        "background_color",
    )
    list_filter = ("added_at", "updated_at", "active")
    search_fields = ("name",)
    date_hierarchy = "updated_at"
