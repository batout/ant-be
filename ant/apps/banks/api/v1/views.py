from django.utils.timezone import now
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets
from rest_framework.filters import SearchFilter
from django.utils.decorators import method_decorator
from django.views.decorators.cache import cache_page
from django.views.decorators.vary import vary_on_cookie

from ant.apps.banks.api.v1.serializers import BankSerializer
from ant.apps.banks.models import Bank
from ant.apps.banks.tasks import refresh_lean_bank_list

class BankViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = BankSerializer
    queryset = Bank.objects.filter(active=True)
    filter_backends = [DjangoFilterBackend, SearchFilter]
    search_fields = ["name"]

    @method_decorator(vary_on_cookie)
    @method_decorator(cache_page(60*60))
    def list(self, request, *args, **kwargs):
        return super().list(request, *args, **kwargs)
    def get_queryset(self):
        if not Bank.objects.filter(updated_at__date__gte=now().date()).exists():
            refresh_lean_bank_list.delay()
        return super(BankViewSet, self).get_queryset()
