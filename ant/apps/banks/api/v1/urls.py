from rest_framework import routers

from ant.apps.banks.api.v1.views import BankViewSet

router = routers.DefaultRouter()
router.register("banks", BankViewSet)

app_name = "banks"

urlpatterns = router.urls
