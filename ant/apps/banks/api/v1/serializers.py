from ant.apps.banks.models import Bank
from ant.utils.serializers import ModelSerializer


class BankSerializer(ModelSerializer):
    class Meta:
        model = Bank
        fields = [
            "id",
            "identifier",
            "name",
            "logo",
            "country_code",
            "active",
            "main_color",
            "background_color",
        ]
