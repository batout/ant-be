from config.celery_app import *
from django.conf import settings
import requests
from .models import Bank


@shared_task
def refresh_lean_bank_list():
    url = settings.LEAN_BASE_URL + "banks/v1"
    headers = {
        "lean-app-token": settings.LEAN_AUTHENTICATION_TOKEN,
        "Content-Type": "application/json",
    }

    response = requests.request("GET", url, headers=headers)
    banks_json = response.json()
    for bank_object in banks_json:
        bank, created = Bank.objects.get_or_create(
            identifier=bank_object["identifier"]
        )
        bank.name = bank_object["name"]
        bank.logo = bank_object["logo"]
        bank.country_code = bank_object["country_code"]
        bank.main_color = bank_object["main_color"]
        bank.background_color = bank_object["background_color"]
        bank.active = bank_object["active"]
        bank.save()
