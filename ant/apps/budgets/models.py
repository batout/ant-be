from django.db import models
from django.utils.translation import ugettext_lazy as _

# Create your models here.
from ant.apps.accounts.models import Account
from ant.apps.categories.models import Category
from ant.apps.currencies.models import Currency
from ant.apps.tags.models import Tag
from ant.apps.users.models import User
from ant.utils.models import BaseModel


class Budget(BaseModel):
    class FrequencyType(models.TextChoices):
        YEARLY = "YEARLY", _("Yearly")
        MONTHLY = "MONTHLY", _("Monthly")
        WEEKLY = "WEEKLY", _("Weekly")
        DAILY = "DAILY", _("Daily")

    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
    name = models.TextField(max_length=100, null=True)
    start_date = models.DateField(null=True)
    accounts = models.ManyToManyField(
        Account, blank=True, help_text=_("if not specified then its general")
    )
    categories = models.ManyToManyField(Category, blank=True)
    tags = models.ManyToManyField(Tag, blank=True)
    amount = models.DecimalField(decimal_places=2, max_digits=14)
    currency = models.ForeignKey(Currency, on_delete=models.CASCADE, null=True)
    repeat = models.BooleanField(default=False)
    end_date = models.DateField(null=True)
    frequency = models.TextField(choices=FrequencyType.choices, null=True)
    rollover = models.BooleanField(default=False)
    icon = models.ImageField(null=True, blank=True)

    def save(self, *args, **kwargs):
        if self.currency is None:
            self.currency = Currency.objects.filter(code="SAR").last()
        super(Budget, self).save(*args, **kwargs)
