from datetime import datetime

import pytz
from django.conf import settings
from rest_framework import serializers

from ant.apps.budgets.models import Budget
from ant.apps.categories.api.v1.serializers import CategorySerializer
from ant.apps.categories.models import Category
from ant.utils.serializers import ModelSerializer
from django.db.models import Sum


class BudgetSerializer(ModelSerializer):
    categories = serializers.PrimaryKeyRelatedField(
        many=True, queryset=Category.objects.all(), required=True, allow_null=False
    )
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    expenses = serializers.SerializerMethodField()
    remaining = serializers.SerializerMethodField()
    remaining_percentage = serializers.SerializerMethodField()

    class Meta:
        model = Budget
        fields = [
            "id",
            "name",
            "start_date",
            "accounts",
            "categories",
            "tags",
            "amount",
            "currency",
            "repeat",
            "end_date",
            "frequency",
            "rollover",
            "categories",
            "icon",
            "user",
            "expenses",
            "remaining",
            "remaining_percentage",
        ]

    def get_expenses(self, budget):
        budget_expenses = 0
        for category in budget.categories.all():
            for transaction in category.transactions.filter(user=budget.user).all():
                budget_expenses += transaction.amount

        if budget_expenses == None:
            budget_expenses = 0
        return budget_expenses

    def get_remaining(self, budget):
        return budget.amount - self.get_expenses(budget)

    def get_remaining_percentage(self, budget):
        return (self.get_remaining(budget) / budget.amount) * 100


class BudgetSerializerDetail(BudgetSerializer):
    categories = CategorySerializer(fields=["id", "title", "icon"], many=True)
