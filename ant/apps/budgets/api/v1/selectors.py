from django.db.models import Sum
from datetime import datetime
from django.conf import settings
import pytz


def single_budget_expenses(budget):
    budget_expenses = 0
    for category in budget.categories.all():
        for transaction in category.transactions.filter(user=budget.user).all():
            budget_expenses += transaction.amount

    if budget_expenses == None:
        budget_expenses = 0
    return budget_expenses


def get_budgets_extra_data(queryset, user):
    amount_sum_of_budget = queryset.aggregate(Sum('amount'))['amount__sum']
    print(amount_sum_of_budget == None)
    if amount_sum_of_budget == None:
        amount_sum_of_budget = 0
    budgets = queryset.all()
    budget_expenses = 0
    for budget in budgets:
        budget_expenses += single_budget_expenses(budget)

    return {
        'total_budgets_amount': amount_sum_of_budget,
        'expenses': budget_expenses,
        'remaining': amount_sum_of_budget - budget_expenses
    }


def get_transactions_date_filters(qs, start_date, end_date):

    if (
        start_date is not None
        and start_date != ""
        and end_date is not None
        and end_date != ""
    ):
        start_date = datetime.fromisoformat(str(start_date))
        end_date = datetime.fromisoformat(str(end_date))
        tz = pytz.timezone(settings.TIME_ZONE)
        start_date = start_date.replace(tzinfo=tz)
        end_date = end_date.replace(tzinfo=tz)
        qs = qs.filter(
            timestamp__gte=start_date, timestamp__lte=end_date
        )
    elif start_date and start_date != "":
        start_date = datetime.fromisoformat(str(start_date))
        tz = pytz.timezone(settings.TIME_ZONE)
        start_date = start_date.replace(tzinfo=tz)
        qs = qs.filter(timestamp__gte=start_date)
    elif end_date and end_date != "":
        end_date = datetime.fromisoformat(end_date)
        tz = pytz.timezone(settings.TIME_ZONE)
        end_date = end_date.replace(tzinfo=tz)
        qs = qs.filter(timestamp__lte=end_date)
    return qs
