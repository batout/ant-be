from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import status, viewsets
from rest_framework.response import Response

from django.db.models import Sum
from ant.apps.budgets.api.v1.serializers import BudgetSerializer, BudgetSerializerDetail
from ant.apps.budgets.models import Budget
from ant.apps.transactions.models import Transaction
from .selectors import *
from django.db.models.functions import Coalesce


class BudgetViewSet(viewsets.ModelViewSet):
    serializer_class = BudgetSerializer
    queryset = Budget.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        "categories__transactions__timestamp": ["gte", "lte", "exact", "gt", "lt"],
    }

    def get_queryset(self):
        # start_date = self.request.GET.get('start_date',None)
        # end_date = self.request.GET.get('end_date',None)

        qs = super(BudgetViewSet, self).get_queryset().filter(user=self.request.user).prefetch_related(
            'categories', 'accounts', 'tags', 'categories__transactions')
        # qs = get_transactions_date_filters(qs, start_date, end_date)
        return qs

    def get_serializer_class(self):
        if self.action in ["retrieve", "list"]:
            return BudgetSerializerDetail
        return super(BudgetViewSet, self).get_serializer_class()

    """
    List a queryset.
    """

    def list(self, request, *args, **kwargs):
        queryset = self.filter_queryset(self.get_queryset())
        start_date = self.request.GET.get("start_date", None)
        end_date = self.request.GET.get("end_date", None)
        page = self.paginate_queryset(queryset)
        extra_data = get_budgets_extra_data(queryset, self.request.user)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(data={'budgets': serializer.data, "extra_data": extra_data})
        serializer = self.get_serializer(queryset, many=True)
        return Response(data={'budgets': serializer.data, "extra_data": extra_data})

    """
    Create a queryset.
    """

    def create(self, request, *args, **kwarg):
        if isinstance(request.data, list):
            serializer = self.get_serializer(data=request.data, many=True)
        else:
            serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )
