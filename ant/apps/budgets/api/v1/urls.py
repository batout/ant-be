from rest_framework import routers

from ant.apps.budgets.api.v1.views import BudgetViewSet

router = routers.DefaultRouter()

router.register("budgets", BudgetViewSet)

app_name = "budgets"

urlpatterns = router.urls
