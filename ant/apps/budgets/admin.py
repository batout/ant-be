# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Budget


@admin.register(Budget)
class BudgetAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "user",
        "name",
        "start_date",
        "amount",
        "currency",
        "repeat",
        "end_date",
        "frequency",
        "rollover",
        "icon",
    )
    list_filter = (
        "added_at",
        "updated_at",
        "user",
        "start_date",
        "currency",
        "repeat",
        "end_date",
        "rollover",
    )
    raw_id_fields = ("accounts", "categories", "tags")
    search_fields = ("name",)
    date_hierarchy = "updated_at"
