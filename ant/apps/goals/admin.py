# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import Goal, UserGoal


@admin.register(Goal)
class GoalAdmin(admin.ModelAdmin):
    list_display = ("id", "added_at", "updated_at", "user", "name", "image")
    list_filter = ("added_at", "updated_at", "user")
    search_fields = ("name",)
    date_hierarchy = "updated_at"


@admin.register(UserGoal)
class UserGoalAdmin(admin.ModelAdmin):
    list_display = (
        "id",
        "added_at",
        "updated_at",
        "user",
        "goal",
        "value",
        "months",
    )
    list_filter = ("added_at", "updated_at", "user", "goal")
    date_hierarchy = "updated_at"
