import decimal

from dateutil.relativedelta import relativedelta
from django.db import models
from django.utils.timezone import now

# Create your models here.
from ant.apps.users.models import User
from ant.utils.models import BaseModel


class Goal(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)
    name = models.CharField(max_length=100)
    image = models.ImageField(blank=True, null=True)


class UserGoal(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    goal = models.ForeignKey(Goal, on_delete=models.CASCADE)
    value = models.DecimalField(decimal_places=2, max_digits=14)
    months = models.PositiveIntegerField(default=6)

    def reached_months(self, date=now()):

        reached_months = min(round((date - self.added_at).days // 30), self.months)
        if reached_months < 0:
            return 0
        return reached_months

    @ property
    def monthly_value(self):
        # use decimal for precision and avoid float errors and round result
        # return round(decimal.Decimal(self.value) / self.months, 2)
        months = self.months
        if self.months == 0:
            months = 1
        return round(decimal.Decimal(self.value) / months)

    def reached_value(self, date=now()):
        months = self.months
        if self.months == 0:
            months = 1
        return round(
            decimal.Decimal(self.value) / months * self.reached_months(date=date)
        )

    @ property
    def is_done(self, date=None):
        if date is None:
            date = now()
        return self.reached_months(date) >= self.months

    @ property
    def reach_date(self):
        # added_at month + months
        return self.added_at + relativedelta(months=self.months)
