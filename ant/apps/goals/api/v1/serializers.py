from datetime import datetime

import pytz
from django.conf import settings
from rest_framework import serializers

from ant.apps.goals.models import Goal, UserGoal
from ant.utils.serializers import ModelSerializer


class GoalSerializer(ModelSerializer):
    class Meta:
        model = Goal
        fields = [
            "id",
            "name",
            "image",
        ]


class UserGoalSerializer(ModelSerializer):
    user = serializers.HiddenField(default=serializers.CurrentUserDefault())
    goal = serializers.JSONField(write_only=True)
    reached_value = serializers.SerializerMethodField()
    reached_months = serializers.SerializerMethodField()

    class Meta:
        model = UserGoal
        fields = [
            "id",
            "added_at",
            "user",
            "goal",
            "value",
            "months",
            "reached_months",
            "monthly_value",
            "reached_value",
            "reach_date",
        ]

    def get_reached_value(self, obj):
        # added_at__lte = self.context["request"].query_params.get("added_at__lte", None)
        # if added_at__lte is not None and added_at__lte != "":
        #     added_at__lte = datetime.fromisoformat(added_at__lte)
        #     tz = pytz.timezone(settings.TIME_ZONE)
        #     added_at__lte = added_at__lte.replace(tzinfo=tz)
        #     return obj.reached_value(date=added_at__lte)
        return obj.reached_value()

    def get_reached_months(self, obj):
        # added_at__lte = self.context["request"].query_params.get("added_at__lte", None)
        # if added_at__lte is not None and added_at__lte != "":
        #     added_at__lte = datetime.fromisoformat(added_at__lte)
        #     tz = pytz.timezone(settings.TIME_ZONE)
        #     added_at__lte = added_at__lte.replace(tzinfo=tz)
        #     return obj.reached_months(date=added_at__lte)
        return obj.reached_months()

    def create(self, validated_data):
        goal = validated_data.pop("goal", None)
        if isinstance(goal, int):
            new_goal = Goal.objects.get(id=goal)
        elif isinstance(goal, dict):
            new_goal = Goal.objects.create(**goal, user=self.context["request"].user, image='default_goal.png')
        else:
            new_goal = None
        validated_data["goal"] = new_goal
        return super(UserGoalSerializer, self).create(validated_data)

    def update(self, instance, validated_data):
        goal = validated_data.pop("goal", None)
        if goal and isinstance(goal, dict):
            if "name" in goal:
                instance.goal.name = goal['name']
            if "image" in goal:
                instance.goal.image = goal['image']
            instance.goal.save()

        return super(UserGoalSerializer, self).update(instance, validated_data)


class UserGoalSerializerDetail(UserGoalSerializer):
    goal = GoalSerializer()
