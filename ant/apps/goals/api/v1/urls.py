from rest_framework import routers

from ant.apps.goals.api.v1.views import GoalViewSet, UserGoalViewSet

router = routers.DefaultRouter()

router.register("goals", GoalViewSet)
router.register("user_goals", UserGoalViewSet)

app_name = "goals"

urlpatterns = router.urls
