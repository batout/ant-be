from django.db.models import Q
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework import viewsets

from ant.apps.goals.api.v1.serializers import (
    GoalSerializer,
    UserGoalSerializer,
    UserGoalSerializerDetail,
)
from ant.apps.goals.models import Goal, UserGoal


class GoalViewSet(viewsets.ReadOnlyModelViewSet):
    serializer_class = GoalSerializer
    queryset = Goal.objects.all()

    def get_queryset(self):
        # filter if goal has no user or user is the same
        return super().get_queryset().filter(Q(user=None) | Q(user=self.request.user))


class UserGoalViewSet(viewsets.ModelViewSet):
    serializer_class = UserGoalSerializer
    queryset = UserGoal.objects.all()
    filter_backends = (DjangoFilterBackend,)
    filterset_fields = {
        "added_at": ["lte", "gte"],
        "goal": ["exact"],
    }

    def get_queryset(self):
        return (
            super(UserGoalViewSet, self).get_queryset().filter(user=self.request.user)
        )

    def get_serializer_class(self):
        if self.action in ["list", "retrieve"]:
            return UserGoalSerializerDetail
        return super(UserGoalViewSet, self).get_serializer_class()
