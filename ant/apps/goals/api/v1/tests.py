"""testing user goals list v1 api"""

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase

from ant.apps.goals.models import Goal, UserGoal
from ant.apps.users.models import User


class UserGoalListV1APITestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            password="password",
            phone_number="+966512345678",
        )
        self.goal = Goal.objects.create(name="goal")
        self.user_goal = UserGoal.objects.create(
            user=self.user,
            goal=self.goal,
            value=100,
            months=6,
        )
        self.url = reverse("goals:usergoal-list")

    def test_user_goal_list_v1_api(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # assume that now time in future after months of user goal
    def test_user_goal_list_v1_api_after_months(self):
        # change user goal added_at time
        from django.utils import timezone

        self.user_goal.added_at = timezone.now() - timezone.timedelta(days=180)
        self.user_goal.save()
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, status.HTTP_200_OK)

    # test filter of added_at
    def test_user_goal_list_v1_api_filter_added_at(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.get(self.url, {"added_at__gte": "2020-01-01T00:00"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)


class UserGoalCreateV1APITestCase(APITestCase):
    def setUp(self):
        self.user = User.objects.create(
            password="password",
            phone_number="+966512345678",
        )
        self.goal = Goal.objects.create(name="goal")
        self.url = reverse("goals:usergoal-list")

    def test_user_goal_create_v1_api(self):
        self.client.force_authenticate(user=self.user)
        data = {"goal": {"name": "goal"}, "value": 100, "months": 6}
        response = self.client.post(self.url, data=data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            UserGoal.objects.get(id=response.json()["data"]["id"]).goal.name,
            data["goal"]["name"],
        )

    def test_user_goal_create_v1_api_with_predefined_goal(self):
        self.client.force_authenticate(user=self.user)
        response = self.client.post(
            self.url,
            data={
                "goal": self.goal.id,
                "value": 100,
                "months": 6,
            },
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            UserGoal.objects.get(id=response.json()["data"]["id"]).goal.name,
            self.goal.name,
        )
