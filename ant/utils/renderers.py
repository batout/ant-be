from rest_framework.renderers import JSONRenderer as _


class JSONRenderer(_):
    def render(self, data, accepted_media_type=None, renderer_context=None):
        if isinstance(data, dict) and data.get("status", None) == 0:
            response = super(JSONRenderer, self).render(
                data, accepted_media_type, renderer_context
            )
            return response
        response_data = {
            "status": 1,
            "status_code": renderer_context["response"].status_code,
            "message": "",
            "data": data,
        }
        if data is None:
            response = super(JSONRenderer, self).render(
                response_data, accepted_media_type, renderer_context
            )
            return response

        if "count" in data and "next" in data and "previous" in data:
            response_data = {
                "status": 1,
                "status_code": renderer_context["response"].status_code,
                "pagination": {
                    "count": data["count"],
                    "current": data["current"],
                    "pages": data["pages"],
                    "next": data["next"],
                    "previous": data["previous"],
                },
                "message": "",
                "data": data["results"],
            }

        if "status" in data and "message" in data:
            if data.get("info"):
                response_data = {
                    "status": data.get("status", 1),
                    "status_code": renderer_context["response"].status_code,
                    "message": data.get("message", ""),
                    "info": data.get("info", {}),
                    "data": data.get("data", {}),
                }
            else:
                response_data = {
                    "status": data.get("status", 1),
                    "status_code": renderer_context["response"].status_code,
                    "message": data.get("message", ""),
                    "data": data.get("data", {}),
                }

        response = super(JSONRenderer, self).render(
            response_data, accepted_media_type, renderer_context
        )
        return response
