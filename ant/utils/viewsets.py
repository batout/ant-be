from rest_framework.viewsets import ModelViewSet as _
from rest_framework_extensions.mixins import DetailSerializerMixin


class ModelViewSet(DetailSerializerMixin, _):
    pass
