from rest_framework import renderers
from rest_framework.views import exception_handler

from ant.utils.renderers import JSONRenderer


def custom_exception_handler(exc, context):
    # Call REST framework's default exception handler first,
    # to get the standard error response.
    if isinstance(context["request"].accepted_renderer, JSONRenderer):
        context["request"].accepted_renderer = renderers.JSONRenderer()
    response = exception_handler(exc, context)
    # Now add the HTTP status code to the response.
    if response is not None:
        flag = True
        errors = []
        try:
            message = response.data.get("detail")
        except:
            message = response.data[0]

        if not message:
            for field, value in response.data.items():
                errors.append("{} : {}".format(field, " ".join(value)))
            if len(response.data.items()) == 1:

                flag = False
                for field, value in response.data.items():
                    message = value
            response.data = {
                "status": 0,
                "status_code": response.status_code,
                "message": "Validation Error" if flag else message[0],
                "errors": errors,
            }
        else:
            response.data = {
                "status": 0,
                "status_code": response.status_code,
                "message": message,
                "errors": [],
            }
    return response
