from django_restql.mixins import DynamicFieldsMixin
from rest_framework.serializers import ModelSerializer as _


class ModelSerializer(DynamicFieldsMixin, _):
    pass
