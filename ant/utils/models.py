from django.db import models
from django.utils.translation import ugettext_lazy as _


class BaseModel(models.Model):
    added_at = models.DateTimeField(_("Added at"), auto_now_add=True)
    updated_at = models.DateTimeField(_("Updated at"), auto_now=True, null=True)

    class Meta:
        abstract = True
