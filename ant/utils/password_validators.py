import re

from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _


class ContainsUpperLowerCaseCharacterValidator:
    def validate(self, password, user=None):
        contains_upper_character = bool(re.match(r"\w*[A-Z]\w*", password))
        contains_lower_character = bool(re.match(r"\w*[a-z]\w*", password))
        if not (contains_upper_character and contains_lower_character):
            raise ValidationError(
                _("This password must contain upper and lower characters."),
            )

    def get_help_text(self):
        return _("Your password must contain upper and lower characters.")


class ContainsSpecialCharacterValidator:
    def validate(self, password, user=None):
        contain_special_character = bool(re.match(r"\w*[*@!#%&()^~_{}]\w*", password))
        if not contain_special_character:
            raise ValidationError(
                _("This password must contain at least one special character."),
            )

    def get_help_text(self):
        return _("Your password must contain at least one special character.")


class NumberValidator:
    def validate(self, password, user=None):
        if not password.isdigit():
            raise ValidationError(
                _("The password must contain digits only"),
                code='password_no_number',
            )

    def get_help_text(self):
        return _(
            "Your password must be digits"
        )
